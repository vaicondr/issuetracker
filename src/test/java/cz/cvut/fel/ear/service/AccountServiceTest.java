package cz.cvut.fel.ear.service;


import cz.cvut.fel.ear.environment.Generator;
import cz.cvut.fel.ear.model.Account;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

public class AccountServiceTest extends BaseServiceTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Test
    public void persistEncodesUserPassword() {
        final Account account = Generator.generateUser();
        final String rawPassword = account.getPassword();
        userService.persist(account);

        final Account result = em.find(Account.class, account.getId());
        assertNotNull(result);
        assertTrue(passwordEncoder.matches(rawPassword, result.getPassword()));
    }

}