package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.environment.Generator;
import cz.cvut.fel.ear.model.Account;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.util.ChartGenerator;
import cz.cvut.fel.ear.util.ChartImageExport;
import org.jfree.chart.JFreeChart;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.util.List;

public class ChartGeneratorTest {

    @PersistenceContext
    private EntityManager em;


    @Autowired
    private UserService userService;

    @Autowired
    private IssueService issueService;


    @Test
    public void generatesChartCorrectly() {
        Account account = Generator.generateUser();
        List<Issue> issue = Generator.generateIssues(5);
        ChartGenerator chartGenerator = new ChartGenerator();
        JFreeChart chart = chartGenerator.createChart(account,issue);
        File outputFile = new File("/tmp/chart.png");
        ChartImageExport.writeAsPNG(chart,outputFile,512,512);

    }
}
