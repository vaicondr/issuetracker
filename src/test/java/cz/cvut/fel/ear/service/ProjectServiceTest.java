package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.environment.Generator;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.Project;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

public class ProjectServiceTest extends BaseServiceTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private IssueService issueService;

    @Test
    public void issueIsAssignedToProject() {
        final Project p = Generator.generateProject();
        final Issue i = Generator.generateIssue(0);

        issueService.persist(i);
        projectService.persist(p);
        projectService.createIssue(p,i);
        Project foundProject = projectService.find(p.getId());
        assertEquals(1,foundProject.getIssues().size());

    }

}