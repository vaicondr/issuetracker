package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.environment.Generator;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.Account;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static org.junit.Assert.*;

public class IssueServiceTest extends BaseServiceTestRunner {

    @PersistenceContext
    private EntityManager em;


    @Autowired
    private IssueService issueService;

    @Autowired
    private UserService userService;

    @Test
    public void assigningUserWorksCoorectly() {
        final Account account = Generator.generateUser();
        final Issue issue = Generator.generateIssue(0);

        issueService.persist(issue);
        userService.persist(account);

        issueService.assign(account,issue);
        Issue updatedIssue = issueService.find(issue.getId());
        assertEquals(account,updatedIssue.getAssignee());
    }

    @Test
    public void addingTimeTest(){
        final Account account = Generator.generateUser();
        final Issue issue = Generator.generateIssue(0);
        Long expectedTime = 1000L;

        issueService.persist(issue);
        userService.persist(account);
        issueService.assign(account,issue);
        Issue updatedIssue = issueService.find(issue.getId());
        updatedIssue.addWorkedTime(expectedTime);
        issueService.persist(updatedIssue);
        updatedIssue = issueService.find(issue.getId());
        assertEquals(expectedTime,updatedIssue.getWorkedTime());

    }

    @Test
    public void changeIssueStateTest() {

    }

}