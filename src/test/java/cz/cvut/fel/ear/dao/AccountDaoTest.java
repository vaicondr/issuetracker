package cz.cvut.fel.ear.dao;

import cz.cvut.fel.ear.environment.Generator;
import cz.cvut.fel.ear.model.Account;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.Assert.*;

public class AccountDaoTest extends BaseDaoTestRunner {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserDao userDao;

    @Test
    public void findByUsernameReturnsPersonWithMatchingUsername() {
        final Account account = Generator.generateUser();
        em.persist(account);

        final Account result = userDao.findByUsername(account.getUserName());
        assertNotNull(result);
        assertEquals(account.getId(), result.getId());
    }

    @Test
    public void findByUsernameReturnsNullForUnknownUsername() {
        assertNull(userDao.findByUsername("unknownUsername"));
    }

    @Test
    public void deleteRemovesUserCorrectly() {
        final Account account = Generator.generateUser();
        em.persist(account);
        em.remove(account);
        assertNull(userDao.findByUsername(account.getUserName()));
    }

    @Test
    public void updatingUserWorksCorrectly() {
        final Account account = Generator.generateUser();
        em.persist(account);
        String email = "testEmail";
        account.setEmail(email);
        em.persist(account);
        assertEquals(email, userDao.findByUsername(account.getUserName()).getEmail());
    }

}