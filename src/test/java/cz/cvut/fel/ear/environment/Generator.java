package cz.cvut.fel.ear.environment;

import cz.cvut.fel.ear.model.Account;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.IssueState;
import cz.cvut.fel.ear.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {

    private static final Random RAND = new Random();

    public static int randomInt() {
        return RAND.nextInt();
    }

    public static long randomLong() {
        return RAND.nextLong();
    }


    public static boolean randomBoolean() {
        return RAND.nextBoolean();
    }

    public static Account generateUser() {
        final Account account = new Account();
        account.setUserName("testName");
        account.setPassword(Integer.toString(randomInt()));
        return account;
    }

    public static Issue generateIssue(long workedTime) {
        final Issue i= new Issue();
        i.setIssueName("Product" + randomInt());
        i.setIssueState(IssueState.PENDING);
        i.setIssueDescription("TEST content");
        i.setWorkedTime(workedTime);
        return i;
    }

    public static List<Issue> generateIssues(int count) {
        List<Issue> issues = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            issues.add(generateIssue(randomLong()));
        }
        return issues;
    }

    public static Project generateProject() {
        final Project p = new Project();
        p.setProjectName("testProject");
        return p;
    }
}
