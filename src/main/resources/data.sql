INSERT INTO public.account (id, email, passwordhash, username) VALUES (1,'johny@email.com','$2a$10$aHahFuHn75uayqZD1s3.vuL5tDP7sUdx0Tu2i9qFHZrQGL3jQ2ARG','John Doe');
INSERT INTO public.usergroup (id, groupname) VALUES (2,"Test group");
INSERT INTO public.allegiance (user_id, usergroup_id) VALUES (1,2);
INSERT INTO public.project (id,projectname,assignedgroup_id) VALUES (3,"Test project",2)

