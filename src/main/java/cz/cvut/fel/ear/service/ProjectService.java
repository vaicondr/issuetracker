package cz.cvut.fel.ear.service;


import cz.cvut.fel.ear.dao.GenericDao;
import cz.cvut.fel.ear.dao.ProjectDao;
import cz.cvut.fel.ear.dao.IssueDao;
import cz.cvut.fel.ear.model.Project;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.UserGroup;
import cz.cvut.fel.ear.service.repository.AbstractRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

@Service
public class ProjectService extends AbstractRepositoryService<Project> {

    private final ProjectDao dao;

    private final IssueDao issueDao;

    @Autowired
    public ProjectService(ProjectDao dao, IssueDao issueDao) {
        this.dao = dao;
        this.issueDao = issueDao;
    }

    /**
     *
     * @param project  Project
     * @param toAdd Item to add
     */
    @Transactional
    public void createIssue(Project project, Issue toAdd) {
        Objects.requireNonNull(project);
        Objects.requireNonNull(toAdd);
        project.addIssue(toAdd);
        dao.update(project);
    }

    /**
     * Removes the specified issue from project.
     * <p>
     *
     * @param project  Target project
     * @param toRemove Item to remove
     */
    @Transactional
    public void removeIssue(Project project, Issue toRemove) {
        Objects.requireNonNull(project);
        Objects.requireNonNull(toRemove);
        project.addIssue(toRemove);
        dao.update(project);
    }

    @Transactional
    public void createProject(String projectName, Optional<UserGroup> userGroup) {
        Project project = new Project();
        project.setProjectName(projectName);
        userGroup.ifPresent(project::setAssignedGroup);
        dao.persist(project);
    }

    @Transactional
    public void assignGroup(Project project,UserGroup userGroup) {
        Objects.requireNonNull(project);
        Objects.requireNonNull(userGroup);
        project.setAssignedGroup(userGroup);
        dao.persist(project);
    }

    @Transactional
    public void removeGroup(Project project) {
        Objects.requireNonNull(project);
        project.setAssignedGroup(null);
        dao.persist(project);
    }


    @Override
    protected GenericDao<Project> getPrimaryDao() {
        return dao;
    }
}
