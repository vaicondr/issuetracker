package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.dao.GenericDao;
import cz.cvut.fel.ear.dao.UserDao;
import cz.cvut.fel.ear.model.Account;
import cz.cvut.fel.ear.model.AccountRole;
import cz.cvut.fel.ear.service.repository.AbstractRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Objects;

@Service
public class UserService extends AbstractRepositoryService<Account> {

    private final UserDao dao;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao dao, PasswordEncoder passwordEncoder) {
        this.dao = dao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected GenericDao<Account> getPrimaryDao() {
        return dao;
    }

    @Transactional
    public void persist(Account account) {
        Objects.requireNonNull(account);
        account.encodePassword(passwordEncoder);
        if (account.getRole() == null) {
            account.setRole(AccountRole.USER);
        }
        dao.persist(account);
    }

    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return dao.findByUsername(username) != null;
    }

    @Transactional
    public void remove(Account account) {
        Objects.requireNonNull(account);
        dao.remove(account);
    }


}
