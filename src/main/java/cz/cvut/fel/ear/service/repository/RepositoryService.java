/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.fel.ear.service.repository;

import java.util.Collection;
import java.util.List;

/**
 * Defines basic CRUD operations for repository-based services.
 *
 * @param <T> Type handled by this service
 */
public interface RepositoryService<T> {

    List<T> findAll();

    T find(Integer id);

    void persist(T instance);

    void persist(Collection<T> instances);

    void update(T instance);

    void remove(T instance);

    /**
     * Checks whether an instance with the specified id exists.
     *
     * @param id Instance identifier
     * @return Whether a matching instance exists
     */
    boolean exists(Integer id);
}
