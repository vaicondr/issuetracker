package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.dao.CommentDao;
import cz.cvut.fel.ear.dao.GenericDao;
import cz.cvut.fel.ear.dao.IssueDao;
import cz.cvut.fel.ear.model.*;
import cz.cvut.fel.ear.service.repository.AbstractRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class IssueService extends AbstractRepositoryService<Issue> {

    private final IssueDao dao;

    private final CommentDao commentDao;

    @Autowired
    public IssueService(IssueDao dao,CommentDao commentDao) {
        this.dao = dao;
        this.commentDao = commentDao;
    }

    @Transactional(readOnly = true)
    public List<Issue> findAll(Project project) {
        return dao.findAll(project);
    }

    /**
     * Asigns account to an issue
     * @param account
     * @param issue
     */
    @Transactional
    public void assign(Account account, Issue issue){
        Objects.requireNonNull(issue);
        Objects.requireNonNull(account);
        issue.setAssignee(account);
        dao.update(issue);
    }

    @Transactional
    public void addTime(Issue issue,long time) {
        Objects.requireNonNull(issue);
        issue.addWorkedTime(time);
        dao.update(issue);
    }

    @Transactional
    public void changeIssueState(Issue issue, IssueState newIssueState) {
        Objects.requireNonNull(issue);
        Objects.requireNonNull(newIssueState);
        issue.setIssueState(newIssueState);
        dao.update(issue);
    }

    @Transactional
    public void postComment(Issue issue, Comment comment) {
        Objects.requireNonNull(issue);
        Objects.requireNonNull(comment);
        commentDao.persist(comment);
        issue.addComment(comment);
        dao.update(issue);
    }

    @Transactional
    public void deleteComment(Issue issue,Comment comment) {
        Objects.requireNonNull(issue);
        Objects.requireNonNull(comment);
        issue.getComments().remove(comment);
        dao.update(issue);
        commentDao.remove(comment);
    }



    @Override
    protected GenericDao<Issue> getPrimaryDao() {
        return dao;
    }
}
