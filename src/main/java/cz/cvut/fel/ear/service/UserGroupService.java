package cz.cvut.fel.ear.service;

import cz.cvut.fel.ear.dao.GenericDao;
import cz.cvut.fel.ear.dao.UserGroupDao;
import cz.cvut.fel.ear.model.Project;
import cz.cvut.fel.ear.model.Account;
import cz.cvut.fel.ear.model.TeamAssociation;
import cz.cvut.fel.ear.model.UserGroup;
import cz.cvut.fel.ear.service.repository.AbstractRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserGroupService extends AbstractRepositoryService<UserGroup> {

    private final UserGroupDao dao;

    @Autowired
    public UserGroupService(UserGroupDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void createNewGroup(String groupName) {
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupName(groupName);
        dao.persist(userGroup);
    }

    @Transactional
    public void removeGroup(UserGroup userGroup) {
        Objects.requireNonNull(userGroup);
        for(TeamAssociation teamAssociation : userGroup.getAccounts()) {
            Account account = teamAssociation.getAccount();
            account.getUserGroups().remove(teamAssociation);
        }
        dao.remove(userGroup);
    }

    @Transactional
    public void addUser(UserGroup userGroup, Account toAdd) {
        Objects.requireNonNull(userGroup);
        Objects.requireNonNull(toAdd);
        userGroup.addUser(toAdd);
        dao.update(userGroup);
    }

    @Transactional
    public void removeUser(UserGroup userGroup, Account toAdd) {
        Objects.requireNonNull(userGroup);
        Objects.requireNonNull(toAdd);
        userGroup.removeUser(toAdd);
        dao.update(userGroup);
    }

    @Transactional
    public List<Account> getAllUsers(UserGroup userGroup) {
        Objects.requireNonNull(userGroup);
        return userGroup.getAccounts()
                .stream()
                .map(TeamAssociation::getAccount)
                .collect(Collectors.toList());
    }

    @Override
    protected GenericDao<UserGroup> getPrimaryDao() {
        return dao;
    }
}
