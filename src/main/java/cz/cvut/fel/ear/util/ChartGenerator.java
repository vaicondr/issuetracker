package cz.cvut.fel.ear.util;

import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.Account;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ChartGenerator {

    public JFreeChart createChart(Account account, List<Issue> issues) {
        JFreeChart chart = ChartFactory.createPieChart(
                account.getUserName() + "'s time spent on issues.",   // chart title
                createDataset(issues),          // data
                true,             // include legend
                true,
                false);
        return chart;
    }

    private PieDataset createDataset(List<Issue> issues) {
        DefaultPieDataset dataset = new DefaultPieDataset( );

        for(Issue issue:issues) {
            dataset.setValue(issue.getIssueName(),issue.getWorkedTime());
        }
        return dataset;
    }
}
