package cz.cvut.fel.ear.util;

import com.itextpdf.awt.DefaultFontMapper;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import cz.cvut.fel.ear.dao.BaseDao;
import org.jfree.chart.JFreeChart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class ChartImageExport {

    protected static final Logger LOG = LoggerFactory.getLogger(ChartImageExport.class);

    public static void writeAsPNG(JFreeChart chart, File out, int width, int height ) {
            BufferedImage chartImage = chart.createBufferedImage( width, height, null);
        try {
            ImageIO.write( chartImage, "png", out );
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

    }

    public static void writeAsPDF( JFreeChart chart, OutputStream out, int width, int height ) {
        try {
            Rectangle pagesize = new Rectangle( width, height );
            Document document = new Document( pagesize, 50, 50, 50, 50 );
            PdfWriter writer = PdfWriter.getInstance( document, out );
            document.open();
            PdfContentByte cb = writer.getDirectContent();
            PdfTemplate tp = cb.createTemplate( width, height );
            Graphics2D g2 = tp.createGraphics( width, height, new DefaultFontMapper() );
            Rectangle2D r2D = new Rectangle2D.Double(0, 0, width, height );
            chart.draw(g2, r2D);
            g2.dispose();
            cb.addTemplate(tp, 0, 0);
            document.close();
        }
        catch (DocumentException e) {
            LOG.error( e.getMessage() );
        }
    }
}
