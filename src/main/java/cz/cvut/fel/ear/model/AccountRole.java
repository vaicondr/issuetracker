package cz.cvut.fel.ear.model;

public enum AccountRole {
    ADMIN("ROLE_ADMIN"), USER("ROLE_USER");

    private final String name;

    AccountRole(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
