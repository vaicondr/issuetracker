package cz.cvut.fel.ear.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class UserGroup extends AbstractEntity {
    @Basic(optional = false)
    @Column(nullable = false)
    private String groupName;

    @JsonIgnore
    @OneToMany(mappedBy = "usergroup")
    private List<TeamAssociation> accounts;

    public void addUser(Account account) {
        Objects.requireNonNull(account);
        TeamAssociation teamAssociation = new TeamAssociation();
        teamAssociation.setAccount(account);
        teamAssociation.setUserGroup(this);
        teamAssociation.setAccountId(account.getId());
        teamAssociation.setUsergroupId(this.getId());
        if(this.accounts == null) {
            this.accounts = new ArrayList<>();
        }

        this.accounts.add(teamAssociation);
        account.getUserGroups().add(teamAssociation);
    }

    public void removeUser(Account account) {
        Objects.requireNonNull(account);
        if (accounts == null) {
            return;
        }

        TeamAssociation toRemove = null;
        for(TeamAssociation teamAssociation : accounts) {
            if(teamAssociation.getAccountId() == account.getId()) {
                toRemove = teamAssociation;
                break;
            }
        }
        if(toRemove == null) {
            return;
        }

        account.getUserGroups().remove(toRemove);
        accounts.remove(toRemove);
    }


    public UserGroup() {

    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<TeamAssociation> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<TeamAssociation> accounts) {
        this.accounts = accounts;
    }
}
