package cz.cvut.fel.ear.model;

import java.io.Serializable;
import java.util.Objects;

public class TeamAssociationId implements Serializable {
    private Integer accountId;
    private Integer usergroupId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamAssociationId that = (TeamAssociationId) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(usergroupId, that.usergroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, usergroupId);
    }
}
