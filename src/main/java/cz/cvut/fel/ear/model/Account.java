package cz.cvut.fel.ear.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Account.findByUsername", query = "SELECT u FROM Account u WHERE u.userName = :username")
})
public class Account extends AbstractEntity{

    @Basic(optional = false)
    @Column(nullable = false)
    private String userName;

    @Basic(optional = false)
    @Column(nullable = false)
    private String password;

    @Basic
    @Column
    private String email;

    @Enumerated(EnumType.STRING)
    private AccountRole role;

    @JsonIgnore
    @OneToMany(mappedBy = "account")
    private List<TeamAssociation> userGroups;

    public Account() {

    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<TeamAssociation> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<TeamAssociation> userGroups) {
        this.userGroups = userGroups;
    }

    public AccountRole getRole() {
        return role;
    }

    public void setRole(AccountRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(userName, account.userName) &&
                Objects.equals(password, account.password) &&
                Objects.equals(email, account.email) &&
                Objects.equals(userGroups, account.userGroups);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, password, email, userGroups);
    }

    public void encodePassword(PasswordEncoder passwordEncoder) {
        this.password = passwordEncoder.encode(password);
    }
}
