package cz.cvut.fel.ear.model;

import javax.persistence.*;

@Entity
public class Comment extends AbstractEntity {
    @Lob
    @Column(nullable = false,length = 512,columnDefinition = "text")
    private String commentContent;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Account postingAccount;

    @ManyToOne
    private Issue issue;

    public Comment() {

    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Account getPostingAccount() {
        return postingAccount;
    }

    public void setPostingAccount(Account postingAccount) {
        this.postingAccount = postingAccount;
    }
}
