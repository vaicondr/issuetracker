package cz.cvut.fel.ear.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "Issue.findByProject", query = "SELECT i from Issue i WHERE i.correspondingProject = :project")
})
public class Issue extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    private String issueName;

    @Lob
    @Column(nullable = false,length = 1024,columnDefinition = "text")
    private String issueDescription;

    @Basic(optional = false)
    @Column(nullable = false)
    private Long workedTime;

    @Enumerated(EnumType.STRING)
    private IssueState issueState;

    @ManyToOne
    private Project correspondingProject;

    @ManyToOne
    @JoinColumn
    private Account assignee;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Comment> comments;

    public Issue() {

    }


    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public void setWorkedTime(Long workedTime) {
        this.workedTime = workedTime;
    }

    public void addWorkedTime(Long workedTime) {
        this.workedTime += workedTime;
    }

    public void setIssueState(IssueState issueState) {
        this.issueState = issueState;
    }

    public void setCorrespondingProject(Project correspondingProject) {
        this.correspondingProject = correspondingProject;
    }

    public void setAssignee(Account assignee) {
        this.assignee = assignee;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        if(this.comments == null) {
            this.comments = new ArrayList<>();
        }
        comments.add(comment);
    }

    public String getIssueName() {
        return issueName;
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public Long getWorkedTime() {
        return workedTime;
    }

    public IssueState getIssueState() {
        return issueState;
    }

    public Project getCorrespondingProject() {
        return correspondingProject;
    }

    public Account getAssignee() {
        return assignee;
    }

    public List<Comment> getComments() {
        return comments;
    }
}
