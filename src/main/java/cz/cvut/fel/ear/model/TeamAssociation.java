package cz.cvut.fel.ear.model;


import javax.persistence.*;

@Entity
@Table(name="TEAM_EMP")
@IdClass(TeamAssociationId.class)
public class TeamAssociation  {
    @Id
    private int accountId;
    @Id
    private int usergroupId;


    @ManyToOne
    @PrimaryKeyJoinColumn(name = "ACCOUNTID",referencedColumnName = "ID")
    private Account account;

    @ManyToOne
    @PrimaryKeyJoinColumn(name = "USERGROUPID",referencedColumnName = "ID")
    private UserGroup usergroup;

    public TeamAssociation() {

    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public long getUsergroupId() {
        return usergroupId;
    }

    public void setUsergroupId(int usergroupId) {
        this.usergroupId = usergroupId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public UserGroup getUserGroup() {
        return usergroup;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.usergroup = userGroup;
    }
}
