package cz.cvut.fel.ear.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Project extends AbstractEntity {
    @Basic(optional = false)
    @Column(nullable = false)
    private String projectName;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    List<Issue> issues;

    @ManyToOne
    @JoinColumn
    UserGroup assignedGroup;

    public Project() {

    }

    public void addIssue(Issue issue) {

        Objects.requireNonNull(issue);
        if (issues == null) {
            this.issues = new ArrayList<>();
        }
        issues.add(issue);
    }

    public void removeIssue(Issue issue) {

        Objects.requireNonNull(issue);
        if (issues == null) {
            return;
        }
        issues.removeIf(i -> Objects.equals(i.getId(),issue.getId()));
    }


    public String getProjectName() {
        return projectName;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setAssignedGroup(UserGroup assignedGroup) {
        this.assignedGroup = assignedGroup;
    }

    public UserGroup getAssignedGroup() {
        return assignedGroup;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }
}
