package cz.cvut.fel.ear.model;

public enum  IssueState {
    PENDING,ACCEPTED,SOLVED,TESTING,TESTED;

    public static String toJson() {
        final StringBuilder sb = new StringBuilder("[");
        for (IssueState s : values()) {
            if (sb.length() > 1) {
                sb.append(',');
            }
            sb.append('\"').append(s.toString()).append('\"');
        }
        sb.append(']');
        return sb.toString();
    }
}
