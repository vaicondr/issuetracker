package cz.cvut.fel.ear.dao;

import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

@Repository
public class IssueDao extends BaseDao<Issue> {


    @PersistenceContext
    private EntityManager em;

    protected IssueDao() {
        super(Issue.class);
    }

    public Issue find(Integer id) {
        Objects.requireNonNull(id);
        return em.find(Issue.class, id);
    }

    public List<Issue> findAll(Project project) {
        Objects.requireNonNull(project);
        return em.createNamedQuery("Issue.findByProject", Issue.class).setParameter("project", project)
                .getResultList();
    }
}
