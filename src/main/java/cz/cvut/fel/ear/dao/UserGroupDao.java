package cz.cvut.fel.ear.dao;


import cz.cvut.fel.ear.model.UserGroup;
import org.springframework.stereotype.Repository;


@Repository
public class UserGroupDao extends BaseDao<UserGroup>{


    public UserGroupDao() {
        super(UserGroup.class);
    }

}
