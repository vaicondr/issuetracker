package cz.cvut.fel.ear.dao;

import cz.cvut.fel.ear.model.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;



@Repository
public class UserDao extends BaseDao<Account> {

    public UserDao() {
        super(Account.class);
    }

    public Account findByUsername(String username) {
        try {
            return em.createNamedQuery("Account.findByUsername", Account.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
