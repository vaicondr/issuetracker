package cz.cvut.fel.ear.dao;

import cz.cvut.fel.ear.model.Project;
import cz.cvut.fel.ear.rest.ProjectController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;


@Repository
public class ProjectDao extends BaseDao<Project>{

    private static final Logger LOG = LoggerFactory.getLogger(ProjectDao.class);

    public ProjectDao() {
        super(Project.class);
    }

    public Project findByProjectname(String projectName) {
        try {
            return em.createNamedQuery("Project.findByProjectname",Project.class)
                    .setParameter("projectname",projectName)
                    .getSingleResult();
        } catch (NoResultException e) {
            LOG.warn("Project with name {} wasnt found",projectName);
            return null;
        }
    }
}
