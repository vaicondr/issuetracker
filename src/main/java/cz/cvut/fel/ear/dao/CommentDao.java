package cz.cvut.fel.ear.dao;

import cz.cvut.fel.ear.model.Comment;
import cz.cvut.fel.ear.model.Issue;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CommentDao extends BaseDao<Comment>  {

    @PersistenceContext
    private EntityManager em;

    protected CommentDao() {
        super(Comment.class);
    }

}
