package cz.cvut.fel.ear.rest;

import cz.cvut.fel.ear.model.Comment;
import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.rest.util.RestUtils;
import cz.cvut.fel.ear.service.IssueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/issue")
public class IssueController {
    private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

    private final IssueService issueService;

    @Autowired
    public IssueController(IssueService issueService) {
        this.issueService = issueService;
    }


    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Issue getIssue(@PathVariable("id") int issueId) {
        return issueService.find(issueId);
    }

    @PostMapping(value = "/{id}/comment")
    public ResponseEntity<Void> postComment(@PathVariable("id") int issueId,
                                            @RequestBody Comment comment) {
        Issue issue = issueService.find(issueId);
        issueService.postComment(issue,comment);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> createIssue(@RequestBody String issueName)
    {
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PostMapping("/{id}")
    public ResponseEntity<Void> changeState(@PathVariable("id") int issueId,
                                            @RequestBody String newIssueState) {
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

}
