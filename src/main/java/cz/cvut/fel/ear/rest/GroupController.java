package cz.cvut.fel.ear.rest;

import cz.cvut.fel.ear.model.Account;
import cz.cvut.fel.ear.model.UserGroup;
import cz.cvut.fel.ear.rest.util.RestUtils;
import cz.cvut.fel.ear.service.ProjectService;
import cz.cvut.fel.ear.service.UserGroupService;
import cz.cvut.fel.ear.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/groups")
public class GroupController {

    private static final Logger LOG = LoggerFactory.getLogger(GroupController.class);

    private final UserGroupService userGroupService;

    private final UserService userService;

    private final ProjectService projectService;

    @Autowired
    public GroupController(UserGroupService userGroupService,UserService userService,ProjectService projectService) {
        this.userService = userService;
        this.userGroupService = userGroupService;
        this.projectService = projectService;
    }


    @PutMapping()
    public ResponseEntity<Void> createNew(String groupName) {
        userGroupService.createNewGroup(groupName);
        LOG.debug("Usergroup {} successfully created.", groupName);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{groupId}")
    public ResponseEntity<Void> removeGroup(@PathVariable("groupId") int groupId) {
        UserGroup userGroup = userGroupService.find(groupId);
        userGroupService.removeGroup(userGroup);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserGroup> getAllGroups() {
        return userGroupService.findAll();
    }

    @PostMapping(value = "/{groupId}/{userId}")
    public ResponseEntity<Void> addUser(@PathVariable("groupId") int groupId,
                                        @PathVariable("userId") int userId) {
        UserGroup userGroup = userGroupService.find(groupId);
        Account account = userService.find(userId);
        userGroupService.addUser(userGroup,account);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{groupId}/{userId}")
    public ResponseEntity<Void> removeUser(@PathVariable("groupId") int groupId,
                                           @PathVariable("userId") int userId) {
        UserGroup userGroup = userGroupService.find(groupId);
        Account account = userService.find(userId);
        userGroupService.removeUser(userGroup,account);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{groupId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Account> getAllUsersFromGroup(@PathVariable("groupId") int groupId) {
        UserGroup userGroup = userGroupService.find(groupId);
        return userGroupService.getAllUsers(userGroup);
    }
}
