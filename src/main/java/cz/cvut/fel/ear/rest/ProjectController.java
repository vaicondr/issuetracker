package cz.cvut.fel.ear.rest;

import cz.cvut.fel.ear.model.Issue;
import cz.cvut.fel.ear.model.Project;
import cz.cvut.fel.ear.model.UserGroup;
import cz.cvut.fel.ear.rest.util.RestUtils;
import cz.cvut.fel.ear.service.IssueService;
import cz.cvut.fel.ear.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projects")
public class ProjectController {
    private static final Logger LOG = LoggerFactory.getLogger(ProjectController.class);

    private final ProjectService projectService;

    private final IssueService issueService;

    @Autowired
    public ProjectController(ProjectService projectService,IssueService issueService) {
        this.projectService = projectService;
        this.issueService = issueService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/new",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createNew(@RequestBody String projectName,
                                          @RequestBody(required = false) UserGroup assignedGroup) {
        projectService.createProject(projectName, Optional.of(assignedGroup));
        LOG.debug("Project {} successfully created.", projectName);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PostMapping(value = "/{projectId}/assignedGroup", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> assignGroupToProject(@PathVariable("projectId") int projectId,
                                                     @RequestBody UserGroup userGroup) {
        Project project = projectService.find(projectId);
        projectService.assignGroup(project,userGroup);
        LOG.debug("Project {} group successfully assigned.", project);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{projectId}/assignedGroup",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteGroupFromProject(@PathVariable("projectId") int projectId) {
        Project project = projectService.find(projectId);
        projectService.removeGroup(project);
        LOG.debug("Project {} group successfully removed.", project);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> getProjects() {
        return projectService.findAll();
    }

    @GetMapping(value = "/{id}/issues",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Issue> getProjectIssues(@PathVariable("id") int projectId) {
        return projectService.find(projectId).getIssues();
    }

    @PutMapping(value = "/{projectId}/issue",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createNewIssueInProject(@PathVariable("projectId") int projectId,
                                                        @RequestBody Issue issue) {
        Project project = projectService.find(projectId);
        projectService.createIssue(project,issue);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{projectId}/{issueId}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deleteIssue(@PathVariable("projectId") int projectId,
                                            @PathVariable("issueId") int issueId) {
        Project project = projectService.find(projectId);
        Issue issue = issueService.find(issueId);
        projectService.createIssue(project,issue);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/");
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

}
