package cz.cvut.fel.ear.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Import({PersistenceConfig.class,RestConfig.class,ServiceConfig.class,SecurityConfig.class})
public class AppConfig {
}
